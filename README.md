## TODO

1. Have a look to examples in `sd.lab.concurrency.ExecutorServicesExamples`
0. Have a look to examples in `sd.lab.concurrency.SplittingComputationsExamples`
0. Have a look to examples in `sd.lab.concurrency.TestAsyncCounter1`
0. Have a look to examples in `sd.lab.concurrency.FuturesExamples`
0. Have a look to examples in `sd.lab.concurrency.PromisesExamples`
0. Have a look to examples in `sd.lab.concurrency.TestAsyncCounter2`

### Exercise 3-1

> __Goal:__ practice with asynchronous programming + learning how to split long-lasting computations in tasks 

1. Have a look to the interface `sd.lab.concurrency.exercise.AsyncFactorialCalculator`
0. Provide an implementation for such an interface
    + Ensure all tests in `sd.lab.concurrency.exercise.TestAsyncCalculator` are satisfied
    
### Exercise 3-2 (Advanced)

> __Goal:__ practice with completable futures + learning how executor services are implemented 

1. Have a look to the interface `sd.lab.concurrency.exercise.SingleThreadedExecutorService`
0. Provide an implementation for such an interface
    + Ensure all tests in `sd.lab.concurrency.exercise.TestSingleThreadedExecutorService` are satisfied